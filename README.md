# README #

O projeto é uma API que fornece recursos necessários para correção de provas. 

# FRAMEWORKS E DEPENDÊNCIAS #

* Nodejs v 12.16.1
* Sqlite  v 4.1.1
* Express v 4.17.1
* Knex v 0.20.13

* nodemon v 2.0.3 - Este apenas no ambiente de desenvolvimento

* chai v 4.2.0 - testes unitarios
* chai-http v 4.3.0 - testes unitarios
* mocha v 7.1.1 - testes unitarios

# PRÉ-REQUISITOS #

* Ter instalado e configurado Nodejs 
* Ter instalado e configurado Sqlite3

# CONFIGURAÇÕES #

### Clone o repositório ###

$ git clone https://DaviDuque@bitbucket.org/DaviDuque/codigos.git

### Instale as dependências ###

$ npm install

### Inicialize a Aplicação  ###

$ npm start

### Executar testes ###

$ npm test

### Acesso as rotas ###

em http://localhost:3333

# ROTAS #

### Rotas principais ###

/correcoes/proxima  -> GET

/correcoes/{idCorrecao } -> POST

/correcoes/reservadas/{idCorrecao} -> POST

/correcoes/reservadas/ -> GET

### Rotas secundarias ###

/provas -> get -> listar todas provas

/provas -> post -> criar provas

/provas/id -> put -> atualizar prova


/solicitacoes -> get -> busca solicitacoes 

/solicitacoes -> post -> cria solicitacoes


/chaves -> get -> busca todas chaves

/chaves -> post -> cria uma chave

/opcoes -> get -> busca todas opcoes

/opcoe -> post -> cria umsa opcao



# AUTOR #

http://daviduque.com.br/