const connection = require('../database/connection');
const crypto = require('crypto');

module.exports = {
  async index(request, response) {
    const provas = await connection('provas').select('*');
    //const provas = await connection('provas').select('*').where('situacao', 'Disponivel');
    console.log(provas);
    return response.json(provas);
  },

  async create(request, response){
    const {id, item, referencia,  situacao } = request.body;

    const sequencial =  crypto.randomBytes(4).toString('HEX');

    //console.log(teste);
    await connection('provas').insert({
        id,
        item, 
        referencia, 
        sequencial, 
        situacao 
  }, 'id')

  return response.json({ id });
  },

  async update(request, response){

    const { id } = request.params;
    const { item, referencia,  situacao } = request.body;

    const sequencial =  crypto.randomBytes(4).toString('HEX');

    if(id == 5){

         return response.status(401).json({error: 'opera~cao nao permitida'});
    }
    await connection('provas').where('id', '=', id)
    .update({
        item, 
        referencia, 
        sequencial, 
        situacao 
  }, 'id')

  return response.json({ id });
  }
}