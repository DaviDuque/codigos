const connection = require('../database/connection');

module.exports = {
  async index(request, response) {
    //const solicitacoes = await connection('solicitacoes').select('*');
    //const provas = await connection('provas').select('*').where('situacao', 'Disponivel');

    const chaves = await connection.table('chaves').innerJoin('provas', 'chaves.provas_id', '=', 'provas.id');
    return response.json(chaves);
  },

  async create(request, response){
    const { titulo, opcoes, provas_id, status } = request.body;

    const [ chaves_id ] = await connection('chaves').insert({
           titulo, 
           opcoes,
           provas_id, 
           status,         
     }, 'chaves_id')

  return response.json({ chaves_id });
  }
}
