const connection = require('../database/connection');

module.exports = {
  async index(request, response) {
    //const solicitacoes = await connection('solicitacoes').select('*');
    //const provas = await connection('provas').select('*').where('situacao', 'Disponivel');

    const solicitacoes = await connection.table('solicitacoes').innerJoin('provas', 'solicitacoes.provas_id', '=', 'provas.id');
    return response.json(solicitacoes);
  },

  async create(request, response){
    const { solicitacao, provas_id } = request.body;

    const [ solicitacoes_id ] = await connection('solicitacoes').insert({
            solicitacao, 
            provas_id,          
     }, 'solicitacoes_id')

  return response.json({ solicitacoes_id });
  }
}