const connection = require('../database/connection');

module.exports = {
  async index(request, response) {
    
    const opcoes = await connection.table('opcoes').innerJoin('chaves', 'opcoes.chaves_id', '=', 'chaves.chaves_id');
    return response.json(opcoes);
  },

  async create(request, response){
    const { valor, descricao, chaves_id } = request.body;

    const [ opcoes_id ] = await connection('opcoes').insert({
        valor, 
        descricao,
        chaves_id,          
     }, 'opcoes_id')

  return response.json({ opcoes_id });
  }
}
