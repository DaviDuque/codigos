const connection = require('../database/connection');

module.exports = {

  /****************************************proxima correcao***************************************/
  async index(request, response) {

    // Busca item
    const provas = await connection('provas')
      .select('*')
      .where('situacao', 'Disponivel')
      .orderBy('sequencial');


    if (provas == '') {
      return response.status(401).json({
        data: null,
        situacao: "ERRO",
        tipo: "SEM_CORRECAO",
        descrição: "Não existem mais correções disponíveis"
      });
    } else {

      registroProvas = provas[0];

      //busca chave relacionada a item
      const chaves = await connection.table('chaves')
        .innerJoin('provas', 'chaves.provas_id', '=', provas[0].id)
        .where({
          situacao: "Disponivel",
          provas_id: provas[0].id
        })
        .orderBy('id');

      //busca opcoes relacionada a chave
      const opcoes = await connection.table('opcoes')
        .innerJoin('chaves', 'chaves.chaves_id', '=', 'opcoes.chaves_id')
        .where('chaves.provas_id', provas[0].id);
      registroOpcoes = opcoes;

      //busca correcoes solicitadas
      const solicitacoes = await connection.table('solicitacoes').innerJoin('provas', 'solicitacoes.provas_id', '=', provas[0].id);


      // montando retorno
      var data = {}, dataOpcoes = [], registroChave = [], y = 0;
      data.id = registroProvas.id;
      data.item = registroProvas.item;
      data.referencia = registroProvas.referencia;
      data.sequencial = registroProvas.sequencial;
      data.solicitacao = solicitacoes != '' ? solicitacoes[0].solicitacao : null;
      data.chave = [];
      data.chave[0] = { "id": 0, "opcoes": [] };
      data.situacao = registroProvas.situacao;

      var chaveRef = -2, j = 0;
      for (var i = 0; i < chaves.length; i++) {
        if (chaves[i].chaves_id != chaveRef) {
          registroChave[j] = { "id": 0, "opcoes": [] };
          registroChave[j]["id"] = chaves[i].chaves_id;
          chaveRef = chaves[i].chaves_id;
          j++;
        }
        delete registroChave[j];
      }


      var novaChave = [], l = 0;

      registroChave.forEach(e => {
        var l = 0;
        novaChave = [];
        for (let j = 0; j < registroOpcoes.length; j++) {
          if (e.id == registroOpcoes[j].chaves_id) {
            if (l == 0) {
              //inicializa indice objeto
              novaChave[l] = { valor: 0, descricao: "" };
              novaChave[l].valor = registroOpcoes[j].valor;
              novaChave[l].descricao = registroOpcoes[j].descricao;
              l++;
            } else {
              //adiciona indice objeto
              novaChave.push({});
              novaChave[l] = { valor: 0, descricao: "" };
              novaChave[l].valor = registroOpcoes[j].valor;
              novaChave[l].descricao = registroOpcoes[j].descricao;
              l++;
            }

          } else { }
        }
        e.opcoes = novaChave;

      });

      data.chave = registroChave;

      return response.json(data);
    }
  },






  /****************************************adiciona correcao***************************************/
  async update(request, response) {

    const { id } = request.params;
    const { chave } = request.body;

    chave.status = "corrigido";



    const chavesCorrecao = await connection.table('chaves')
      .where({
        chaves_id: chave[0].id,
        provas_id: id
      });


    if (chavesCorrecao == '') {
      const chavesCorrecao2 = await connection.table('chaves')
        .where({
          provas_id: id
        });
      if (chavesCorrecao2 != '') {
        return response.status(404).json({
          "situacao": "ERRO",
          "tipo": "CHAVE_INCORRETA",
          "descrição": `Chave de correção incorreta. Valor ${chave[0].id} não é válido para o item ${id}`
        });
      } else {
        return response.status(404).json({
          "situacao": "ERRO",
          "tipo": "ITEM_INVALIDO",
          "descrição": "Item inválido para correção"
        });
      }

    } else if (chavesCorrecao[0].status == "corrigido") {
      return response.status(404).json({
        "situacao": "ERRO",
        "tipo": "ITEM_CORRIGIDO",
        "descrição": "Item já corrigido"
      });
    } else {


      const opcaoCorrecao = await connection.table('opcoes')
        .where({
          chaves_id: chave[0].id,
          valor: chave[0].valor
        });

      const AddCor = await connection('chaves').where('chaves_id', '=', chave[0].id)
        .update({
          status: chave.status,
          opcoes: opcaoCorrecao[0].opcoes_id
        }, 'chaves_id')


      if (AddCor == 1) {

        return response.json({
          "situacao": "SUCESSO",
          "descrição": "Correção salva com sucesso"
        });
      } else {
        return response.status(401).json({
          "situacao": "ERRO",
          "tipo": "Desconhecido",
          "descrição": "Erro desconhecido"
        });
      }
    }
  },





  /****************************************adiciona correcao  reservada***************************************/

  async updateReservada(request, response) {

    const { id } = request.params;
    const { chave } = request.body;

    chave.status = "reservada";



    const chavesCorrecao = await connection.table('chaves')
      .where({
        chaves_id: chave[0].id,
        provas_id: id,
      });


    if (chavesCorrecao == '') {
      const chavesCorrecao2 = await connection.table('chaves')
        .where({
          provas_id: id
        });
      if (chavesCorrecao2 != '') {
        return response.status(404).json({
          "situacao": "ERRO",
          "tipo": "CHAVE_INCORRETA",
          "descrição": `Chave de correção incorreta. Valor ${chave[0].id} não é válido para o item ${id}`
        });
      } else {
        return response.status(404).json({
          "situacao": "ERRO",
          "tipo": "ITEM_INVALIDO",
          "descrição": "Item inválido para correção"
        });
      }

    } else if (chavesCorrecao[0].status == "corrigido") {
      return response.status(404).json({
        "situacao": "ERRO",
        "tipo": "ITEM_CORRIGIDO",
        "descrição": "Item já corrigido"
      });
    }
    else {


      const opcaoCorrecao = await connection.table('opcoes')
        .where({
          chaves_id: chave[0].id,
          valor: chave[0].valor
        });


      const AddCor = await connection('chaves').where('chaves_id', '=', chave[0].id)
        .update({
          status: chave.status,
        }, 'chaves_id')


      if (AddCor == 1) {



        return response.json({
          "situacao": "SUCESSO",
          "descrição": "Correção reservada com sucesso"
        });
      } else {
        return response.status(401).json({
          "situacao": "ERRO",
          "tipo": "Desconhecido",
          "descrição": "Erro desconhecido"
        });
      }
    }
  },







  /****************************************busca todas as correções reservadas************************************/

  async lista(request, response) {

    const lista = await connection.table('chaves')
      .innerJoin('provas', 'chaves.provas_id', '=', 'provas.id')
      .where({
        status: "reservada"
      })
      .distinct('provas_id');
    if (lista == '') {
      return response.status(401).json({
        "situacao": "ERRO",
        "tipo": "Não encontrado",
        "descrição": "Nenhuma correção reservada encontrada"
      });
    } else {
      novaLista = [];

      for (let i = 0; i < lista.length; i++) {
        novaLista[i] = await listaItem(lista[i].provas_id);
      }

      return await response.json(novaLista);
    }

  }
}



listaItem = async function (item) {
  registroProvas = item;

  //busca chave relacionada a item
  const provasRes = await connection.table('provas')
    .where({ id: registroProvas });

  const solicitacao = await connection.table('solicitacoes').innerJoin('provas', 'solicitacoes.provas_id', '=', item);



  //montagem objeto
  var data = {}, dataOpcoes = [], registroChave = [], y = 0;
  data.id = provasRes[0].id;
  data.item = provasRes[0].item;
  data.referencia = provasRes[0].referencia;
  data.sequencial = provasRes[0].sequencial;
  data.solicitacao = solicitacao != '' ? solicitacao[0].solicitacao : null;
  data.chave = [];
  data.chave[0] = { "id": 0, "opcoes": [] };
  data.situacao = provasRes[0].situacao;

  const chavesRes = await connection.table('chaves')
    .innerJoin('provas', 'chaves.provas_id', '=', 'provas.id')
    .where({
      status: "reservada",
      id: registroProvas,
    });


  var novaChave = [], l = 0;

  const opcoes = await connection.table('opcoes')
    .innerJoin('chaves', 'opcoes.chaves_id', '=', 'chaves.chaves_id')
    .where({
      status: "reservada",
    });

  chavesRes.forEach(async e => {
    delete e.id;
    e.id = e.chaves_id;
    delete e.chaves_id;
    delete e.provas_id;
    delete e.item;
    delete e.referencia;
    delete e.sequencial;
    delete e.situacao;

    data.chave = chavesRes;


    var l = 0;
    novaChave = [];

    for (let j = 0; j < opcoes.length; j++) {
      if (e.id == opcoes[j].chaves_id) {
        if (l == 0) {
          //inicializa indice objeto
          novaChave[l] = { valor: 0, descricao: "" };
          novaChave[l].valor = opcoes[j].valor;
          novaChave[l].descricao = opcoes[j].descricao;
          l++;


        } else {
          //adiciona indice objeto
          novaChave.push({});
          novaChave[l] = { valor: 0, descricao: "" };
          novaChave[l].valor = opcoes[j].valor;
          novaChave[l].descricao = opcoes[j].descricao;
          l++;
        }

      } else {

      }
      delete e.opcoes;
      console.log(" nova Chave", novaChave);
      e.opcoes = novaChave;
    }

  })


  return data;


}
