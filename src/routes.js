const express = require('express');
const ProvasController = require('./controllers/ProvasController');
const SolicitacoesController = require('./controllers/SolicitacoesController');
const ChavesController = require('./controllers/ChavesController');
const OpcoesController = require('./controllers/OpcoesController');
const CorrecoesController = require('./controllers/CorrecoesController');
const routes = express.Router();

routes.get('/provas', ProvasController.index );
routes.post('/provas', ProvasController.create);
routes.put('/provas/:id', ProvasController.update);


routes.get('/solicitacoes', SolicitacoesController.index );
routes.post('/solicitacoes', SolicitacoesController.create);


routes.get('/chaves', ChavesController.index );
routes.post('/chaves', ChavesController.create);

routes.get('/opcoes', OpcoesController.index );
routes.post('/opcoes', OpcoesController.create);

routes.get('/correcoes/proxima', CorrecoesController.index );
routes.post('/correcoes/:id', CorrecoesController.update );
routes.post('/correcoes/reservadas/:id', CorrecoesController.updateReservada );
routes.get('/correcoes/reservadas', CorrecoesController.lista );



module.exports = routes;