
exports.up = function(knex) {
    return knex.schema.createTable('provas', function (table) {
        table.increments('id').primary();
        table.string('item').notNullable();
        table.string('referencia').notNullable();
        table.string('sequencial').notNullable();
        table.string('situacao');
      })
};

exports.down = function(knex) {
    return knex.schema.dropTable('provas');
};
