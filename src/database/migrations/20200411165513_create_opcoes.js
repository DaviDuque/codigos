
exports.up = function(knex) {
    return knex.schema.createTable('opcoes', function (table) {
        table.increments('opcoes_id').primary();
        table.string('valor').notNullable();
        table.string('descricao').notNullable();
        table.integer('chaves_id').notNullable();

        table.foreign('chaves_id').references('chaves_id').inTable('chaves');
      })
};

exports.down = function(knex) {
    return knex.schema.dropTable('opcoes');
};

