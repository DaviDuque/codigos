exports.up = function(knex) {
    return knex.schema.createTable('chaves', function (table) {
        table.increments('chaves_id').primary();
        table.string('titulo').notNullable();
        table.integer('provas_id').notNullable();
        table.integer('opcoes');
        table.string('status');

        table.foreign('provas_id').references('id').inTable('provas');
      })
};

exports.down = function(knex) {
    return knex.schema.dropTable('chaves');
};