
exports.up = function(knex) {
    return knex.schema.createTable('solicitacoes', function (table) {
        table.increments('solicitacoes_id').primary();
        table.string('solicitacao').notNullable();
        table.integer('provas_id').notNullable();

        table.foreign('provas_id').references('id').inTable('provas');
      })
};

exports.down = function(knex) {
    return knex.schema.dropTable('solicitacoes');
};

