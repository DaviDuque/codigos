const chai = require('chai');
const expect = chai.expect;

const somarNum = (a, b) => {
    if (typeof a === "number" && typeof b === "number") return a + b;
    else return undefined;
}

const eliminaRepetidos = (chave) => {
    chaveFinal = [];
    chave.forEach((item) => {
        var duplicated = chaveFinal.findIndex(redItem => {
            return item == redItem;
        }) > -1;
        if (!duplicated) {
            chaveFinal.push(item);
        }
    });
    console.log("chaveFinal", chaveFinal);
    return chaveFinal;
}


describe('Soma', () => {
    it('Soma dois numeros - -2 e 3', (done) => {
        const resultado = somarNum(2, 3);
        expect(resultado).be.equals(5);
        done();
    })

    it('Soma dois numeros - 2 e 3', (done) => {
        const resultado = somarNum(-2, 3);
        expect(resultado).be.equals(1);
        done();
    })

    it('Soma dois numeros - "2" e 3', (done) => {
        const resultado = somarNum("2", 3);
        expect(resultado).be.equals(undefined);
        done();
    })

    it('Soma dois numeros - Null e 3', (done) => {
        const resultado = somarNum(null, 3);
        expect(resultado).be.equals(undefined);
        done();
    })

    /*it('eliminaRepetidos - [1, 2, 1, 4, 5, 2, 2, 5]', (done) => {
        novoArray = [1, 2, 1, 4, 5, 2, 2, 5];
        arrayResultado = [1, 2, 4, 5];
        const resultado = eliminaRepetidos(novoArray);
        expect(resultado).be.equals(arrayResultado);
        done();
    })*/
})

